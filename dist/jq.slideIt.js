/*!
 * jQuery slideIt
 * One of the simplest slideshow plugins for jQuery
 * Note: Source code is written in ES6, use Babel to transpile
 * Copyright (c) 2015 Fahad Hossain <8bit.demoncoder@gmail.com> (github: @fa7ad)
 * License: MIT
 */

"use strict";

(function ($) {
  // Add the method to jQuery
  $.fn.slideIt = function () {
    var argument = arguments[0] === undefined ? {
      time: 500,
      images: ["http://placehold.it/800/400"],
      maxHeight: "300px"
    } : arguments[0];

    // Making sure all the properties are present
    if (!argument.hasOwnProperty("time")) {
      argument.time = 500;
    } else if (!argument.hasOwnProperty("images")) {
      argument.images = ["http://placehold.it/800/200"];
    } else if (!argument.hasOwnProperty("maxHeight")) {
      argument.maxHeight = "300px";
    }
    // define the variable
    var imgElements = "";
    // add the placeholder image's source to the list if it exists
    if (this.find("img").length >= 1) {
      this.find("img").toArray().forEach(function (element) {
        argument.images.push($(element).attr("src"));
      });
    }
    // Generate image markup
    argument.images.forEach(function (image) {
      imgElements += "<img class=\"img-responsive\" src=\"" + image + "\" alt=\"" + image + "\">";
    });
    // Replace the existing markup using the generated ones
    this.html(imgElements);
    // The old variable is not necessary any more
    // Replace its value with an array of all the img elements
    imgElements = this.find("img").toArray();
    // Hide and add css if there are more than 1 elements
    if (imgElements.length > 1) {
      for (var _i = 0; _i < imgElements.length; _i++) {
        $(imgElements[_i]).hide().css("max-height", argument.maxHeight);
      }
    }
    var i = 0;
    // Switch out visible image at interval
    setInterval(function () {
      //this.find("img:visible").slideUp(argument.time * 0.2);
      $(imgElements[i++]).slideDown(argument.time * 0.2).delay(argument.time).slideUp(argument.time * 0.2);
      if (i === imgElements.length) {
        i = 0;
      }
    }, argument.time * 1.2);
    // return what we received to make it chainable
    return this;
  };
})(jQuery);