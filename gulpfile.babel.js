import gulp from "gulp";
import plumber from "gulp-plumber";
import uglify from "gulp-uglify";
import stylus from "gulp-stylus";
import babel from "gulp-babel";
import rename from "gulp-regex-rename";
import fs from "fs";

gulp.task("transpile", () => {
  return gulp.src("src/*.es6.js")
    .pipe(plumber())
    .pipe(babel())
    .on("error", (err) => {
      console.error(`${err.name} on ${err.fileName}`, err.message);
    })
    .pipe(rename(/\.es6/, ""))
    .pipe(gulp.dest("dist"));
});

gulp.task("minify", () => {
  return gulp.src("src/*.es6.js")
    .pipe(plumber())
    .pipe(babel())
    .on("error", (err) => {
      console.error(`${err.name} on ${err.fileName}`, err.message);
    })
    .pipe(uglify({"preserveComments": "some"}))
    .on("error", (err) => {
      console.error(err);
    })
    .pipe(rename(/\.es6\.js$/, ".min.js"))
    .pipe(gulp.dest("dist"));
});

gulp.task("jquery", () => {
  let jQuery = fs.readFileSync("./node_modules/jquery/dist/jquery.min.js");
  return fs.writeFileSync("static/js/jquery.min.js", jQuery);
});

gulp.task("stylus", () => {
  return gulp.src("static/styl/**/*.styl")
    .pipe(plumber())
    .pipe(stylus())
    .pipe(gulp.dest("static/css"))
});

gulp.task("watch", () => {
  gulp.watch("src/**/*.js", ["transpile", "minify"]);
});

gulp.task("watch+static", () => {
  gulp.watch("src/**/*.js", ["transpile", "minify"]);
  gulp.watch("static/styl/**/*.styl", ["stylus"]);
});

gulp.task("default", ["transpile", "minify"], () => {
  gulp.start("watch");
});

gulp.task("static", ["transpile", "minify", "jquery", "stylus"], () => {
  gulp.start("watch+static");
});
