/*eslint-env jquery, browser*/
(function($) {
  "use strict";

  function checkLocation() {
    switch (window.location.hash) {
      case "#/index.html":
        $(".main-container").load("static/index.html");
        $("nav a.active").removeClass("active");
        $("nav a[href*=\"index\"]").addClass("active");
        break;
      case "#/demo1.html":
        $(".main-container").load("static/demo1.html");
        $("nav a.active").removeClass("active");
        $("nav a[href*=\"demo1\"]").addClass("active");
        break;
      case "#/demo2.html":
        $(".main-container").load("static/demo2.html");
        $("nav a.active").removeClass("active");
        $("nav a[href*=\"demo2\"]").addClass("active");
        break;
      default:
        $(".main-container").load("static/index.html");
        $("nav a.active").removeClass("active");
        $("nav a[href*=\"index\"]").addClass("active");
        break;

    }
  }
  $(window).load(function() {
    checkLocation();
  });
  $("nav").on("click", "a", function() {
    checkLocation();
  });

})(jQuery);
