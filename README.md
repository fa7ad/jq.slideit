# jQuery.slideIT
---
One of the simplest slideshow plugins for jQuery

## Usage:

The slideIt method takes one parameter.

The parameter should be an object with the properties `time`, `images` and `maxHeight`.

`time`: interval between each slide, in **miliseconds**

`images`: array of links to images.

`maxHeight`: can be a number (eg. 300) or a string (eg. "350px"). This is to reduce the "resize" effect on swapping images, use **"auto"** to disable this.

## Example:

```javascript
$("div.slider").slideIt({
  time: 5000,
  images: ["1.jpg", "2.jpg"],
  maxHeight: $(".container").height()
});
```
